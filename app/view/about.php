
<div id="content">
	<div class="aboutus">
		<div class="row">
			<h1>ABOUT US</h1>
			<img src="public/images/content/content-truck.png" alt="Truck">
			<div id="companyInfo">
				<div class="rightCont fr">
					<?php include 'form.php';?>
				</div>
			</div>
			<p>Florida #1 Junk Removal is dedicated to recycling and removing trash with prompt and excellent customer service. We provide removal services for foreclosures, demolition clean out, junk removal, furniture removal, appliance removal and much more.</p>
			<p>Florida #1 Junk Removal was founded in Delray Beach, Florida and services most of Broward and Palm Beach counties. We recycle objects and dispose of trash, debris, electronics, appliances or most any kind of JUNK!. Florida #1 Junk Removal is licensed and insured in Junk Removal and all our personnel are insured as well. We provide junk hauling services at very affordable rates, and believe in building our business one customer at a time. Text us a photo and we will give you a quick estimate, schedule a date & time for removal and provide a courteous & professional team to take your junk away!</p>
			<p>Our large and advanced technology trucks get the job done efficiently. We offer many services for our residential, construction, and commercial customers. Everything is loaded onto the trucks, hauled away, and the area is cleaned up when all the junk is gone! No job is too big or too small. Whether it's a ton of clutter in your garage, construction debris, old electronics or appliances, or a foreclosure property that needs to be totally cleaned out, we can handle the job!</p>
			<p>Our goal is always 100% customer satisfaction.</p>
			<p>Contact us today and experience the difference!</p>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
