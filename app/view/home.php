<div id="companyInfo">
	<div class="row">
		<div class="leftCont fl">
			<h1>FLORIDA #1 JUNK REMOVAL</h1>
			<p>Florida #1 Junk Removal was founded in Delray Beach, Florida and services most of Broward and Palm Beach counties. We recycle objects and dispose of trash, debris, electronics, appliances or most any kind of JUNK!. Florida #1 Junk Removal is licensed and insured in Junk Removal and all our personnel are insured as well. We provide junk hauling services at very affordable rates, and believe in building our business one customer at a time. Text us a photo and we will give you a quick estimate, schedule a date & time for removal and provide a courteous & professional team to take your junk away!</p>
			<div class="btn">
				<a href="<?php echo URL ?>about#content">LEARN MORE</a>
			</div>
		</div>
    <div class="rightCont fr">
			<?php include 'form.php';?>
		</div>
    <div class="clearfix"></div>
	</div>
</div>
<div id="content">
	<div class="row">
		<div class="contTop fr">
			<div class="paraLeft">
				<p>CHOOSE DATE<img src="public/images/content/check.png" alt="checkmark"></p>
				<p>APPROVE WRITTEN ESTIMATE<img src="public/images/content/check.png" alt="checkmark"></p>
				<p>RECEIVED CALL BEFORE ARRIVAL<img src="public/images/content/check.png" alt="checkmark"></p>
			</div>
			<div class="cols col-1">
				<div class="contInner">
					<p class="aboutHD">100% Customer Satisfaction</p>
					<p class="aboutInfo">Florida #1 Junk Removal is dedicated to recycling and removing trash with prompt and excellent customer service. We provide removal services for foreclosures, demolition clean out, junk removal, furniture removal, appliance removal and much more.</p>
					<a href="<?php echo URL ?>about#content">Learn More</a>
				</div>
			</div>
			<div class="cols col-3">
				<img src="public/images/content/about-img2.png" alt="contBot Image">
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="services">
	<div class="row">
		<h2>WE CAN TAKE IT ALL</h2>
		<div class="servicesList">
			<div class="cols col-1">
				<ul>
					<li>TRASH REMOVAL</li>
					<li>FURNITURE REMOVAL</li>
					<li>DEBRIS REMOVAL</li>
					<li>JUNK REMOVAL</li>
				</ul>
			</div>
			<div class="cols col-2">
				<ul>
					<li>COMMERCIAL JUNK REMOVAL</li>
					<li>FORECLOSE CLEAN OUT</li>
					<li>DEMOLITION CLEAN OUT</li>
					<li>APPLIANCE REMOVAL</li>
				</ul>
			</div>
			<div class="cols col-3">
				<ul>
					<li>CONSTRUCTION CLEAN OUT</li>
					<li>HOT TUB DISPOSAL</li>
					<li>YARD WASTE REMOVAL</li>
					<li>MATTRESS DISPOSAL</li>
				</ul>
			</div>
		</div>
		<h3><a href="<?php echo URL ?>contact#content">SET APPOINTMENT NOW</a></h3>
	</div>
</div>
