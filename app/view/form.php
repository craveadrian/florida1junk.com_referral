
      <h2>BOOK ONLINE SAVE 20%</h2>
      <form action="sendContactForm" method="post"  class="sends-email ctc-form" >
      <label><span class="ctc-hide">FIRST AND LAST NAME</span>
        <input type="text" name="name" placeholder="FIRST AND LAST NAME">
      </label>
      <label><span class="ctc-hide">ADDRESS</span>
        <input type="text" name="address" placeholder="ADDRESS">
      </label>
      <div class="middleInputs">
        <label><span class="ctc-hide">ZIP CODE</span>
          <input type="text" name="zip" placeholder="ZIP CODE">
        </label>
        <label><span class="ctc-hide">Phone</span>
          <input type="text" name="phone" placeholder="PHONE">
        </label>
        <label><span class="ctc-hide">DATE</span>
          <input type="text" name="date" placeholder="DATE">
        </label>
        <label><span class="ctc-hide">TIME</span>
          <input type="text" name="time" placeholder="TIME">
        </label>
      </div>
      <label><span class="ctc-hide">MESSAGE</span>
        <textarea name="message" cols="30" rows="10" placeholder="MESSAGE:"></textarea>
      </label>
      <label><span class="ctc-hide">Recaptcha</span></label>
      <div class="g-recaptcha"></div>
      <label>
        <input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
      </label><br>
      <?php if( $this->siteInfo['policy_link'] ): ?>
      <label>
        <input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
      </label>
      <?php endif ?>
      <button type="submit" class="ctcBtn" disabled>SCHEDULE</button>
    </form>
    
