<div id="content">
	<div class="service">
		<div class="row">
			<h1>WE CAN TAKE IT ALL!</h1>
			<div id="companyInfo">
				<div class="rightCont fr">
					<?php include 'form.php';?>
				</div>
			</div>
			<ul>
				<li>CONSTRUCTION WASTE REMOVAL</li>
				<li>TELEVISION DISPOSAL & RECYCLING</li>
				<li>ELECTRONIC WASTE DISPOSAL</li>
				<li>FORECLOSURE CLEAN OUT & REMOVAL</li>
				<li>REFRIGERATOR DISPOSAL & RECYCLING</li>
				<li>APPLIANCE REMOVAL</li>
				<li>FURNITURE REMOVAL</li>
				<li>HOT TUB DISPOSAL</li>
				<li>GARBAGE REMOVAL</li>
				<li>YARD WASTE REMOVAL</li>
				<li>TRASH REMOVAL</li>
				<li>MATTRESS DISPOSAL</li>
			</ul>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
